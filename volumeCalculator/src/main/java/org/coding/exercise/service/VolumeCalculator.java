package org.coding.exercise.service;

/**
 * @author Orest Lozynskyy
 * @since 2017-11-11
 */
public interface VolumeCalculator {
    Integer calculate(Integer[] inputArray);
}
