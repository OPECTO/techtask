package org.coding.exercise.service.impl;

import org.coding.exercise.service.VolumeCalculator;
import org.springframework.stereotype.Component;

/**
 * An implementation of VolumeCalculator.class
 *
 * @author Orest Lozynskyy
 * @since 2017-11-11
 */
@Component
public class VolumeCalculatorImpl implements VolumeCalculator {

    /**
     * Calculate a volume according to input array
     *
     * @param inputArray input array of integers
     * @return calculated volume
     */
    @Override
    public Integer calculate(Integer[] inputArray) {
        int volume = 0;
        int max = Integer.MIN_VALUE;

        if (inputArray.length <= 2) {
            return volume;
        }

        int candVolume = 0;
        int candCount = 0;
        int candMax = Integer.MIN_VALUE;

        int tempVolume = 0;
        int tempCount = 0;
        int tempMax = Integer.MIN_VALUE;
        int tempPrev = Integer.MIN_VALUE;


        int input;
        for (int i = 0; i < inputArray.length; i++) {
            input = inputArray[i];
            if (input >= max) {
                volume += (candVolume + (max - candMax) * candCount) + tempVolume;
                max = input;

                candVolume = 0;
                candCount = 0;
                candMax = Integer.MIN_VALUE;

                tempVolume = 0;
                tempCount = 0;
                tempMax = Integer.MIN_VALUE;
                tempPrev = Integer.MIN_VALUE;
            } else if (input >= tempMax && input > tempPrev) {
                if (input == candMax && candMax != Integer.MIN_VALUE && tempMax == Integer.MIN_VALUE) {
                    candCount++;
                } else if (input > candMax && (tempMax != Integer.MIN_VALUE || candMax != Integer.MIN_VALUE)) {
                    candVolume += (tempVolume - (max - input) * tempCount) + ((input - candMax) * candCount);
                    candMax = input;
                    candCount += tempCount + 1;

                    tempVolume = 0;
                    tempCount = 0;
                    tempMax = Integer.MIN_VALUE;
                    tempPrev = Integer.MIN_VALUE;
                } else if (input == candMax) {
                    candVolume += (tempVolume - (max - input) * tempCount) ;
                    candCount += tempCount + 1;

                    tempVolume = 0;
                    tempCount = 0;
                    tempMax = Integer.MIN_VALUE;
                    tempPrev = Integer.MIN_VALUE;
                } else {
                    tempMax = input;
                    tempVolume += (max - input);
                    tempCount++;
                    tempPrev = input;
                }
            } else if (input <= tempMax) {
                tempVolume += max - input;
                tempPrev = input;
                tempCount++;
            }


            if (i == inputArray.length - 1) {
                volume += candVolume;
            }
        }

        return volume;
    }
}
