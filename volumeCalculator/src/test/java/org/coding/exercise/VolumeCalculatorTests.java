package org.coding.exercise;

import org.coding.exercise.service.VolumeCalculator;
import org.coding.exercise.service.impl.VolumeCalculatorImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Simple unit tests for VolumeCalculator.class implementations
 *
 * @author Orest Lozynskyy
 * @since 2017-11-11
 */
public class VolumeCalculatorTests {

    private VolumeCalculator volumeCalculator = new VolumeCalculatorImpl();

    private Map<Integer[], Integer> data = new HashMap<>();

    {
        /*
         *           +
         *           +         +
         *           +       + +
         *           + + +   + +
         */
        data.put(new Integer[]{4, 1, 1, 0, 2, 3}, 8);

        /*
         *           +         +
         *           +       + +
         *           + + +   + +
         */
        data.put(new Integer[]{3, 1, 1, 0, 2, 3}, 8);

        /*
         *           +       +
         *           +       +
         *           + + + + +
         */
        data.put(new Integer[]{3, 1, 1, 1, 3}, 6);

        /*
         *                     +
         *           +         +
         *           +       + +
         *           + + +   + +
         */
        data.put(new Integer[]{3, 1, 1, 0, 2, 4}, 8);

        /*
         *           +
         *           + +
         *           + + +
         *           + + + +
         */
        data.put(new Integer[]{1, 3, 4, 2}, 0);

        /*
         *           + + + +
         *           + + + +
         *           + + + +
         */
        data.put(new Integer[]{3, 3, 3, 3}, 0);

        /*
         *           +
         *           +   +
         *           + + +   +
         *           + + + + +
         */
        data.put(new Integer[]{3, 2, 4, 1, 2}, 2);

        /*
         *           +
         *           +   +
         *           + + +   +
         *           + + +   + + +
         */
        data.put(new Integer[]{1, 2, 3, 0, 4, 2, 1}, 3);

        /*
         *           +
         *           +
         *           + + + +         + +   + +
         *           + + + + + + + + + + + + + + + +
         */
        data.put(new Integer[]{4, 2, 2, 2, 1, 1, 1, 1, 2, 2, 1, 2, 2, 1, 1, 1}, 5);

        /*
         *           +
         *           +             +
         *           +     +   +   +
         *           + + + + + + + +
         */
        data.put(new Integer[]{4, 1, 1, 2, 1, 2, 1, 3}, 10);

        /*
         *                                               +
         *           +                                 + +
         *           +                 +     +         + +
         *           +     +       +   +   + + + +     + +       +
         *           + + + + + + + + + + + + + + + + + + + + + + + +
         */
        data.put(new Integer[]{4, 1, 1, 2, 1, 1, 1, 2, 1, 3, 1, 2, 3, 2, 2, 1, 1, 4, 5, 1, 1, 1, 2, 1}, 42);
    }

    /*
     * Using one test with data you lose ability to run test for particular case from dataset
     */
    @Test
    public void check_algorithm_for_correctness() {
        for (Map.Entry<Integer[], Integer> entry : data.entrySet()) {
            Integer[] inputArray = entry.getKey();

            Integer volume = volumeCalculator.calculate(inputArray);

            Assert.assertEquals(entry.getValue(), volume);
        }
    }

}
