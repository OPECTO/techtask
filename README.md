### How do I get set up? ###

To start an app execute maven command:

 **`mvn clean spring-boot:run`**

 To call an app make http POST request to **`http://{appHost}:{appPort}/volume/calculator`** endpoint. For example:

**`POST http://localhost:8080/volume/calculator`**

With input array of integers in the request body for example:

**`[4, 1, 1, 2, 1, 1, 1, 2, 1, 3, 1, 2, 3, 2, 2, 1, 1, 4, 5, 1, 1, 1, 2, 1]`**

The computation complexity of **`calculate`** method from **`VolumeCalculatorImpl.class`** is **`O(n)`**.
The algorithm needs of memory is approximately **`(N + 10) * 4 byte`**, excluding of Java references overhead and additional memory for Web layer and Spring DI etc.
