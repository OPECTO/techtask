package org.coding.exercise;

import org.coding.exercise.config.VolumeCalculatorWebApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Simple integration test for VolumeCalculatorRestController.class
 *
 * @author Orest Lozynskyy
 * @since 2017-11-12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = VolumeCalculatorWebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VolumeCalculatorRestControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void greetingShouldReturnDefaultMessage() throws Exception {
        String url = "http://localhost:" + port + "/volume/calculator";
        Integer[] inputArray = {4, 1, 1, 2, 1, 1, 1, 2, 1, 3, 1, 2, 3, 2, 2, 1, 1, 4, 5, 1, 1, 1, 2, 1};

        Integer response = this.restTemplate.postForObject(url, inputArray, Integer.class);

        assertThat(response).isEqualTo(42);
    }
}
