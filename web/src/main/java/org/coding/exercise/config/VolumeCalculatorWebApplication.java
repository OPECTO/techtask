package org.coding.exercise.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "org.coding.exercise.*")
public class VolumeCalculatorWebApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(VolumeCalculatorWebApplication.class, args);
	}
}
