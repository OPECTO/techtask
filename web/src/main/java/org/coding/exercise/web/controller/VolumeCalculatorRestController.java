package org.coding.exercise.web.controller;

import org.coding.exercise.service.VolumeCalculator;
import org.coding.exercise.web.validation.VolumeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Controller that make available to calculate volume via http call
 *
 * @author Orest Lozynskyy
 * @since 2017-11-11
 */
@RestController
@RequestMapping(path = "/volume/calculator")
public class VolumeCalculatorRestController {

    private VolumeCalculator volumeCalculator;

    @Autowired
    public VolumeCalculatorRestController(VolumeCalculator volumeCalculator) {
        this.volumeCalculator = volumeCalculator;
    }

    /**
     * Endpoint to calculate volume via http call
     *
     * @param inputArray input array of integers
     * @return calculated volume
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Integer calculateVolume(@RequestBody(required = false) Integer[] inputArray) throws Exception {
        VolumeValidator.validate(inputArray);

        return volumeCalculator.calculate(inputArray);
    }
}
