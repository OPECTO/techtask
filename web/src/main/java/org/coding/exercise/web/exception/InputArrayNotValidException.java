package org.coding.exercise.web.exception;

/**
 * @author Orest Lozynskyy
 * @since 2017-11-18
 */
public class InputArrayNotValidException extends VolumeException {
    public InputArrayNotValidException(String message) {
        super(message);
    }

    public InputArrayNotValidException(String message, Throwable cause) {
        super(message, cause);
    }
}
