package org.coding.exercise.web.exception;

/**
 * @author Orest Lozynskyy
 * @since 2017-11-18
 */
public class VolumeException extends RuntimeException {
    public VolumeException(String message) {
        super(message);
    }
    public VolumeException(String message, Throwable cause) {
        super(message, cause);
    }
}
