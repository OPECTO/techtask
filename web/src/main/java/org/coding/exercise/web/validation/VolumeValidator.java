package org.coding.exercise.web.validation;

import org.coding.exercise.web.exception.InputArrayNotValidException;

/**
 * Simple validator for org.coding.exercise.service.VolumeCalculator#calculate(java.lang.Integer[])
 *
 * @author Orest Lozynskyy
 * @since 2017-11-18
 */
public class VolumeValidator {

    public static void validate(Integer[] inputArray) {
        if (inputArray == null) {
            throw new InputArrayNotValidException("Input array should be not null");
        }
        if (inputArray.length == 0) {
            throw new InputArrayNotValidException("Input array should be not empty");
        }
    }
}
