package org.coding.exercise.web.exception.handling;

import org.coding.exercise.web.exception.InputArrayNotValidException;
import org.coding.exercise.web.exception.VolumeException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * @author Orest Lozynskyy
 * @since 2017-11-18
 */
@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { InputArrayNotValidException.class})
    protected ResponseEntity<Object> handleConflict(VolumeException ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(),
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = { VolumeException.class})
    protected ResponseEntity<Object> handleConflict1(VolumeException ex, WebRequest request) {
        String bodyOfResponse = "Sorry but your data is not correct...";
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = { RuntimeException.class, Exception.class })
    protected ResponseEntity<Object> handleUnexpectedCheckedException(Exception ex, WebRequest request) {
        String bodyOfResponse = "Sorry but we have some problems on our side!\n:(\nPlease try again later...";
        return handleExceptionInternal(ex, bodyOfResponse,
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
